"use strict";

// https://www.programiz.com/javascript/variables-constants

// ----------------------------------------------------------------------------
// DEMO 1 (prototype)

const data = {
  manufacturer: "Volkswagen",
  model: "Golf VII",
};

function Car(manufacturer, model) {
  this.manufacturer = manufacturer;
  this.model = model;

  this.getFullname = function () {
    return `${this.manufacturer} ${this.model}`;
  };
}

const myCar = new Car(data.manufacturer, data.model);

console.log(myCar.__proto__ === Car.prototype);
console.log(typeof Car);
console.log(typeof Car.prototype);

// https://stackoverflow.com/questions/9959727/proto-vs-prototype-in-javascript

// ----------------------------------------------------------------------------
// DEMO 2 (bind)

var obj1 = {
  count: 10,
  doSomethingLater: function () {
    setTimeout(function () {
      // the function executes on the window scope
      this.count++;
      console.log(this);
      console.log(this.count);
    }, 500);
  },
};

var obj2 = {
  count: 20,
  doSomethingLater: function () {
    setTimeout(() => {
      this.count++;
      console.log(this);
      console.log(this.count);
    }, 1000);
  },
};

obj1.doSomethingLater();
obj2.doSomethingLater();
