#include <cstdio>
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

const char SYMBOL_SPACE = '*';
const char SYMBOL_TREE = ' ';

void christmasTree(int size)
{
    int rows = 1;

    if (size >= 3)
    {
        rows = size - 2;
    }

    int cols = rows * 2 + 1;

    string line;

    line.append(cols, SYMBOL_SPACE);
    cout << line << endl;

    for (int i = rows; i > 0; i--)
    {
        line = "";
        line.append(i, SYMBOL_SPACE);
        line.append(cols - 2 * i, SYMBOL_TREE);
        line.append(i, SYMBOL_SPACE);
        cout << line << endl;
    }

    line = "";
    line.append(cols, SYMBOL_SPACE);
    cout << line << endl;
}

int main(int argc, char *argv[])
{
    int size = 10;

    if (argc > 1)
    {
        size = atoi(argv[1]);
    }

    christmasTree(size);
}
