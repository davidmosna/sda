'use strict';

const FILLING = {
  TREE = '*',
  SPACE = ' ',
};

const christmasTree = (size) => {
  const rows = size >= 3 ? size - 2 : 1;
  const cols = rows * 2 + 1;

  console.log(FILLING.SPACE.repeat(cols));

  let line;

  for (let i = rows; i > 0; i--) {
    line = "";
    line += FILLING.SPACE.repeat(i);
    line += FILLING.TREE.repeat(cols - 2 * i);
    line += FILLING.SPACE.repeat(i);
    console.log(line);
  }

  console.log(FILLING.SPACE.repeat(cols));
};

// demo test
christmasTree(10);
