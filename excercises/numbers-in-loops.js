"use strict";

const numbersInLoops = (start, end, modulo) => {
  const numbers = [];

  while (start !== end) {
    if (start % modulo === 0) {
      numbers.push(start);
    }

    start++;
  }

  console.log(numbers.join(", "));
};

// demo test
numbersInLoops(10, 20, 3);
